
var Clustering = function () {
  // Data points. This array will contain points (array of two elements).
  // Example:
  //
  // data = [[0, 0], [4, 5], ...];
  this.data = [];

  // Clusters. Array of arrays containing points. Each cluster is associated with
  // its array index (e.g. cluster 0 is index 0), which is an array of points.
  //
  // Starts empty. Must call cluster() to fill.
  this.clusters = [];

  //////////////////////////
  // TODO: Implement.
  //////////////////////////
  // Clusters this.data into this.clusters. Clears any old this.clusters state.
  this.cluster = function (numClusters) {
    // Below is a dumb implementation. You can do better!
    //
    // Assign points to a random cluster.

    this.clusters = [];
    for (var i = 0; i < numClusters; ++i) this.clusters.push([]);

    for (var i in this.data) {
      var datum = this.data[i];
      this.clusters[randomInt(0, numClusters)].push(datum);
    }
  };


  //////////////////////////
  // Utilty functions. You may find them useful.
  //////////////////////////

  // Returns distance between two points.
  this.euclidDistance = function (p1, p2) {
    return Math.sqrt(Math.pow(p1[0] - p2[0], 2) + Math.pow(p1[1] - p2[1], 2));
  };

  // Random number, min (inclusive) to max (exclusive).
  this.randomInt = function (min, max) {
    var diff = max - min;
    return min + Math.floor(Math.random() * diff);
  };

  // Data is an array of [x, y] coordinates.
  this.importData = function (data) {
    this.data = [];
    for (var i in data) {
      this.data.push([parseInt(data[i][0]), parseInt(data[i][1])]);
    }
  };

  return this;
};
